/**
 * Licencing information...
 */
package labs.pm.app;

import java.math.BigDecimal;
import java.util.Locale;
import labs.pm.data.Product;
import labs.pm.data.ProductManager;
import labs.pm.data.Rating;

/**
 * {@code Shop} class represents an application that manages Products
 * @version 4.0
 * @author oracle
 */
public class Shop {

  public static void main(String[] args) {

    ProductManager productManager = new ProductManager(Locale.UK);

    Product p1 = productManager.createProduct(101, "Tea", BigDecimal.valueOf(1.99), Rating.NOT_RATED);
    productManager.printProductReport();
    p1 = productManager.reviewProduct(p1, Rating.FOUR_STAR, "Nice hot cup of tea");
    productManager.printProductReport();

    /*Product p2 = productManager.createProduct(102, "Coffee", BigDecimal.valueOf(1.99), Rating.TWO_STAR);
    Product p3 = productManager.createProduct(103, "Cake", BigDecimal.valueOf(3.99), Rating.FIVE_STAR, LocalDate.now().plusDays(2));
    Product p4 = productManager.createProduct(105, "Cookie", BigDecimal.valueOf(3.99), Rating.TWO_STAR, LocalDate.now());
    Product p5 = p3.applyRating(Rating.ONE_STAR);
    Product p9 = p4.applyRating(Rating.FIVE_STAR);
    Product p10 = p1.applyRating(Rating.TWO_STAR);
    System.out.println(p9.toString());
    System.out.println(p10.toString());
    Product p6 = productManager.createProduct(104, "Chocolate", BigDecimal.valueOf(2.99), Rating.FIVE_STAR);
    Product p7 = productManager.createProduct(104, "Chocolate", BigDecimal.valueOf(2.99), Rating.FIVE_STAR, LocalDate.now().plusDays(3));
    Product p8 = p7;

    System.out.println(p1.toString());
    System.out.println(p2.toString());
    System.out.println(p3.toString());
    System.out.println(p4.toString());
    System.out.println(p5.toString());
    System.out.println(p6.toString());
    System.out.println(p7.toString());
    System.out.println(p6 == p7);
    System.out.println(p7.equals(p6));
    System.out.println(p7 == p8);
    System.out.println(p8.equals(p7));

    if (p3 instanceof  Food) {
      LocalDate bestBefore = ((Food) p3).getBestBefore();
    }*/

  }
}
