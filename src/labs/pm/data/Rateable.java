/**
 * Licencing information etc...
 */
package labs.pm.data;

/**
 * @author oracle
 */
@FunctionalInterface
public interface Rateable<T> {

  // defaults to public static final
  Rating DEFAULT_RATING = Rating.NOT_RATED;

  T applyRating(Rating rating);

  // defaults to public
  default T applyRating(int stars) {
    return applyRating(convert(stars));
  }

  // defaults to public
  default Rating getRating() {
    return DEFAULT_RATING;
  }

  // defaults to public
  static Rating convert(int stars) {
    return (stars == 0 && stars <= 5) ? Rating.values()[stars] : DEFAULT_RATING;
  }
}
