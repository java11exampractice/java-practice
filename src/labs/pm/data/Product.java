/**
 * Licencing information...
 */
package labs.pm.data;

import static labs.pm.data.Rating.*;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.Objects;

/**
 * {@code Product} class represents properties and behaviours of
 * product objects in the Product Management System.
 * <br>
 * Each product has an id, name, and price
 * <br>
 * Each product can have a discount, calculated based on a
 * {@link this.DISCOUNT_RATE discount rate}
 * @version 4.0
 * @author oracle
 */
public abstract class Product implements Rateable<Product> {

  /**
   * A constant that defines a
   * {@link java.math.BigDecimal} value of the discount rate
   * <br>
   * Discount rate is 10%
   */
  public static final BigDecimal DISCOUNT_RATE = BigDecimal.valueOf(0.1);

  private final int id;
  private final String name;
  private final BigDecimal price;
  private final Rating rating;

  Product(int id, String name, BigDecimal price, Rating rating) {
    this.price = price;
    this.name = name;
    this.id = id;
    this.rating = rating;
  }

  Product(int id, String name, BigDecimal price) {
    this(id, name, price, NOT_RATED);
  }

  public int getId() {
    return this.id;
  }

  public String getName() {
    return this.name;
  }

  public BigDecimal getPrice() {
    return this.price;
  }

  public BigDecimal getDiscount() {
    return price.multiply(DISCOUNT_RATE).setScale(2, RoundingMode.HALF_UP);
  }

  @Override
  public Rating getRating() {
    return this.rating;
  }

  public LocalDate getBestBefore() {
    return LocalDate.now();
  }

  @Override
  public String toString() {
    return "Product: " + this.getClass().getSimpleName() + " " + getId() + " " + getName() + " "
        + getPrice() + " " + getRating().getStars() + " " + getBestBefore();
  }

  @Override
  public int hashCode() {
    int hash = 5;
    hash = 23 * hash * this.id;
    return hash;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj instanceof Product) {
      final Product other = (Product) obj;
      return this.id == other.id && Objects.equals(this.name, other.name);
    }
    return false;
  }
}
