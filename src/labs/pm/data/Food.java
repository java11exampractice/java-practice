/**
 * Licence information...
 */
package labs.pm.data;

import java.math.BigDecimal;
import java.time.LocalDate;
import org.jetbrains.annotations.NotNull;

/**
 * @author oracle
 */
public final class Food extends Product {

  /**
   * Best Before date of food object
   */
  private LocalDate bestBefore;

  /**
   * Initializes the food object
   * @param bestBefore
   */
  Food(LocalDate bestBefore, int id, String name, BigDecimal price, Rating rating) {
    super(id, name, price, rating);
    this.bestBefore = bestBefore;
  }

  @Override
  public LocalDate getBestBefore() {
    return bestBefore;
  }

  @Override
  public BigDecimal getDiscount() {
    return (bestBefore.isEqual(LocalDate.now())) ? super.getDiscount() : BigDecimal.ZERO;
  }

  @NotNull
  @Override
  public String toString() {
    return super.toString() + " " + bestBefore;
  }

  @Override
  public Product applyRating(Rating newRating)  {
    return new Food(bestBefore, getId(), getName(), getPrice(), newRating);
  }
}
